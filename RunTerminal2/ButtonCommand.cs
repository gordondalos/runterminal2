﻿namespace RunTerminal2
{
    public class ButtonCommand
    {
        public ButtonCommand()
        {
            Button = "";
            Command = "";
        }

        public string Button { get; set; }
        public string Command { get; set; }
    }
}
