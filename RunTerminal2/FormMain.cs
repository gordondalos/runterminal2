﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace RunTerminal2
{
    public partial class FormMain : Form
    {
        public FormMain()
        {
            try
            {
                //loading configuration
                ConfigParser cnf = ConfigParser.GetInst();
                cnf.LoadConfiguration("Config.txt");


                //rendering form
                var lstBC = cnf.GetButtonCommands();
                // Считаем скольк кнопок чтобы расчитать размеры формы и сделать массив с информацией о кнопках, и задаем 
                // масиив определенного размера.
                var arrButtons = new Button[lstBC.Count];

                int i = 0;
                foreach (var bc in lstBC)
                {
                    arrButtons[i] = new Button
                    {
                        Width = 200,
                        Height = 32,
                        Name = bc.Command,
                        Location = new Point(20, 32*(i + 1)),
                        Text = bc.Button
                    };
                    arrButtons[i].Click += CommonButtonClickEvent;
                    Controls.Add(arrButtons[i]);

                    i++;
                }
                // инициализируем форму
                InitializeComponent();
            }
            catch (Exception ex)
            {
                ShowCritErrMessage("CRTICAL_ERROR: " + ex.Message);
            }
        }

        public static void ShowCritErrMessage(string msg)
        {
            MessageBox.Show(msg, "Err!", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        /// <summary>
        ///     Данная функция получает обьект (кнопку) и событие.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void CommonButtonClickEvent(object sender, EventArgs e)
        {
            //getting command line string
            string command = ((Control) sender).Name;


            //Запускаем командную строку и передаем ей команду, командная строка остается открытой
            // /K заменить на /С если нужно чтобы командная строка закрылась после исполнения
            var p = new Process
            {
                StartInfo =
                {
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    FileName = "cmd.exe",
                    Arguments = "/C " + command
                }
            };


            try
            {
                p.Start();
            }
            catch (Exception ex)
            {
                ShowCritErrMessage("CRTICAL_ERROR: " + ex.Message);
            }
        }

        /// <summary>
        ///     В этом методе конфигурируются параметры формы
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormMain_Load(object sender, EventArgs e)
        {
            var btnsQuantity = ConfigParser.GetInst().GetButtonCommands().Count;
            Height = 50 + btnsQuantity * 32 + 50;
        }
    }
}