﻿using System;
using System.Collections.Generic;
using System.IO;

namespace RunTerminal2
{
    /// <summary>
    ///     Configuration file parser
    /// </summary>
    public class ConfigParser
    {

        #region Singleton implementation

        private static ConfigParser _instance;

        public static ConfigParser GetInst()
        {
            return _instance ?? (_instance = new ConfigParser());
        }

        private ConfigParser()
        {
        }

        #endregion


        // тут хранится список кнопок
        private List<ButtonCommand> _buttonCommands = new List<ButtonCommand>();

        // Absolute or relative path to config file
        public string ConfigFilePath { get; set; }


        private static bool IsConfigFileValid(string fPath)
        {
            // проверяем наличие файла конфигураций
            return File.Exists(fPath);

            //todo: add antoher file checks here, for example file syntax
        }


        /// <summary>
        ///     Load configuration from text file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public void LoadConfiguration(string filePath = null)
        {
            ConfigFilePath = filePath ?? ConfigFilePath;

            if (!IsConfigFileValid(filePath))
                throw new FileLoadException("Wrong config file path: " + filePath);

            using (var sr = new StreamReader(ConfigFilePath))
            {
                var lc = 0; //line counter
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    ++lc;
                    ProcessConfigFileLine(line, lc);
                }
            }
        }

        /// <summary>
        /// Will process config file line
        /// 
        /// All lines, starting from # - are comments, ignoring them
        /// </summary>
        /// <param name="line"></param>
        /// <param name="lineNumber"></param>
        /// <exception cref="Exception">If config-file format is wrong</exception>
        private void ProcessConfigFileLine(string line, int lineNumber)
        {
            // проверяем коментарий это выделенный решеткой или нет
            if (line.StartsWith("#"))
                return;

            //ignore empty lines
            if ("" == line.Replace(@"/\s*/g", ""))
                return;
            
            // разбиваем полученную строку по вериткальному слешу
            var fields = line.Split('|');
            //string a = fields.Count().ToString();
            // создаю новый экземпляр батонокоманд
            var bc = new ButtonCommand();
            try
            {
                bc.Button = fields[0];
                bc.Command = fields[1];
                _buttonCommands.Add(bc);
            }
            catch(Exception ex)
            {
                throw new Exception("Cannot parse config file, line: " + lineNumber + "; cause: " + ex.Message);
            }
        }

        public List<ButtonCommand> GetButtonCommands()
        {
            return _buttonCommands;
        }
    }
}